
// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/.cache/dev-404-page.js")),
  "component---src-pages-about-us-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/about-us.js")),
  "component---src-pages-big-data-analysis-startup-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/big-data-analysis-startup.js")),
  "component---src-pages-blog-details-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/blog-details.js")),
  "component---src-pages-blog-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/blog.js")),
  "component---src-pages-case-studies-details-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/case-studies-details.js")),
  "component---src-pages-case-studies-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/case-studies.js")),
  "component---src-pages-coming-soon-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/coming-soon.js")),
  "component---src-pages-contact-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/contact.js")),
  "component---src-pages-countries-we-worked-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/countries-we-worked.js")),
  "component---src-pages-course-details-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/course-details.js")),
  "component---src-pages-courses-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/courses.js")),
  "component---src-pages-data-analytics-ai-startup-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/data-analytics-ai-startup.js")),
  "component---src-pages-data-analytics-ml-consulting-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/data-analytics-ml-consulting.js")),
  "component---src-pages-data-science-ml-company-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/data-science-ml-company.js")),
  "component---src-pages-data-science-online-courses-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/data-science-online-courses.js")),
  "component---src-pages-digital-marketing-agency-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/digital-marketing-agency.js")),
  "component---src-pages-event-details-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/event-details.js")),
  "component---src-pages-events-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/events.js")),
  "component---src-pages-faq-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/faq.js")),
  "component---src-pages-gallery-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/gallery.js")),
  "component---src-pages-history-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/history.js")),
  "component---src-pages-index-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/index.js")),
  "component---src-pages-machine-learning-ai-solutions-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/machine-learning-ai-solutions.js")),
  "component---src-pages-membership-levels-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/membership-levels.js")),
  "component---src-pages-privacy-policy-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/privacy-policy.js")),
  "component---src-pages-profile-authentication-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/profile-authentication.js")),
  "component---src-pages-seo-agency-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/seo-agency.js")),
  "component---src-pages-service-details-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/service-details.js")),
  "component---src-pages-services-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/services.js")),
  "component---src-pages-team-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/team.js")),
  "component---src-pages-terms-of-service-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/terms-of-service.js")),
  "component---src-pages-testimonials-js": preferDefault(require("/Users/eduardoavila/Desktop/codevs/src/pages/testimonials.js"))
}

