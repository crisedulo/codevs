import React from 'react'
import footerMap from "../../src/assets/images/footer-map.png"

const countriesWeWorked = () => {

    const currentYear = new Date().getFullYear();

    return (
        <section className="footer-area bg-color">
            <div className="container">
                <div className="row">
                    <div className='col-lg-6 col-md-5'>
                        <div className='why-choose-us-content'>
                            <h2>¿Por qué elegirnos?</h2>
                            <p>
                                Somos un equipo con alta experiencia en el desarrollo de
                                software con tecnologías de vanguadia, cada proyecto es un reto
                                que cumplimos con excelencia.
                            </p>
                            {/* <p>
                                Brindamos una excelente atención personalizada porque sabemos lo
                                importante que es la innovación, apoyo y ayuda cuando el
                                cliente, nuestro activo más importante, lo necesita. Nos
                                esmeramos en presentar siempre una pronta respuesta y una
                                efectiva solución a los inconvenientes que pudieran presentarse.
                             </p> */}


                        </div>
                    </div>
                    <div className="col-lg-5 col-sm-10">
                        <div className="single-footer-widget section-title">
                            <br></br> 
                            <h3>Países con los que hemos trabajado</h3>

                            <ul className="footer-contact-info ">
                                <li>
                                    Guatemala
                                </li>
                                <li>
                                    Costa Rica
                                </li>
                                <li>
                                    México
                                </li>
                                <li>
                                    Colombia
                                </li>
                                <li>
                                    Estados Unidos
                                </li>
                                <li>
                                República Dominicana
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <div className="footer-map">
                <img src={footerMap} alt="footer-logo" />
            </div>
        </section>
    );
}

export default countriesWeWorked;