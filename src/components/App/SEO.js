import React from 'react'
import { Helmet } from "react-helmet"

const SEO = () => {
    return (
        <div>
            <Helmet>
                <title>CoDevs</title>
                <meta name="description" content="CoDevs - ¡Transforma tu empresa a la era digital!" />
                <meta name="og:title" property="og:title" content="CoDevs - Start Up IT Company Guatemala"></meta>
                <meta name="twitter:card" content="CoDevs"></meta>
                <link rel="canonical" href="https://codevs-staging.vercel.app"></link>
                <meta property="og:image" content="https://res.cloudinary.com/dev-empty/image/upload/v1593069801/explore-learning.jpg" />
            </Helmet>
        </div>
    )
}

export default SEO
