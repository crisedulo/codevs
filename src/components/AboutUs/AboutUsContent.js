import React from 'react'
import { Link } from 'gatsby'
import aboutImage from '../../assets/images/about/about-img5.png'
import starIcon from '../../assets/images/star-icon.png'
import icon4 from '../../assets/images/icons/icon4.png'
import icon5 from '../../assets/images/icons/icon5.png'
import icon6 from '../../assets/images/icons/icon6.png'
import icon7 from '../../assets/images/icons/icon7.png'
import shape1 from '../../assets/images/shape/circle-shape1.png'

const AboutUsContent = () => {
  return (
    <section className='about-area ptb-100'>
      <div className='container-fluid'>
        <div className='row align-items-center'>
          <div className='col-lg-6 col-md-12'>
            <div className='about-image'>
              <img src={aboutImage} alt='banner' />
            </div>
          </div>

          <div className='col-lg-6 col-md-12'>
            <div className='about-content'>
              <div className='content'>
                <span className='sub-title'>
                  <img src={starIcon} alt='banner' />
                  Sobre Nosotros
                </span>
                <h2>CoDevs</h2>
                <p>
                  CoDevs es una empresa de tecnología dedicada a la resolución
                  de problemas y construcción de productos a través de
                  tecnología a la vanguardia, es un equipo multidisciplinario de
                  profesionales talentosos que aman la tecnología y brindan un
                  enfoque más amplio a cada proyecto, desarrollando soluciones
                  de vanguardia y automatización de procesos.
                </p>

                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='circle-shape1'>
        <img src={shape1} alt='banner' />
      </div>

      <div className='container'>
        <div className='about-inner-area'>
          <div className='row'>
            <div className='col-lg-4 col-md-6 col-sm-6'>
              <div className='about-text'>
                <h3>Nuestra Misión</h3>
                <p>
                  Desarrollar una empresa dedicada a la resolución de problemas
                  y construcción de software hecho a la medida, apoyados en
                  tecnologías a la vanguardia y ejecutando los proyectos
                  utilizando buenas prácticas en las fases de desarrollo,
                  enfatizando la automatización como proceso habitual de la
                  empresa para el desarrollo de soluciones.
                </p>

                <ul className='features-list'>
                  <li>
                    <i className='flaticon-tick'></i> Automatización
                  </li>
                  <li>
                    <i className='flaticon-tick'></i> Talento Humano
                  </li>
                  <li>
                    <i className='flaticon-tick'></i> Buenas Practicas
                  </li>
                </ul>
              </div>
            </div>

            <div className='col-lg-4 col-md-6 col-sm-6'>
              <div className='about-text'>
                <h3>Nuestra Visión</h3>
                <p>
                  Ser una empresa líder en la implementación de soluciones
                  innovadoras que ayuden a alcanzar una alta competitividad en
                  el mercado de nuestros clientes.
                </p>

                <ul className='features-list'>
                  <li>
                    <i className='flaticon-tick'></i> Resultados
                  </li>
                  <li>
                    <i className='flaticon-tick'></i> Innovación
                  </li>
                  <li>
                    <i className='flaticon-tick'></i> Alta Calidad
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='circle-shape1'>
        <img src={shape1} alt='banner' />
      </div>
    </section>
  )
}

export default AboutUsContent
