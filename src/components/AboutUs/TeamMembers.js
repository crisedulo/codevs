import React from 'react'
import { Link } from 'gatsby'
import starIcon from '../../assets/images/star-icon.png'
import scientist1 from '../../assets/images/scientist/scientist1.png'
import scientist2 from '../../assets/images/scientist/scientist2.png'
import scientist3 from '../../assets/images/scientist/scientist3.png'
import scientist4 from '../../assets/images/scientist/scientist4.png'

const TeamMembers = () => {
  return (
    <section className='scientist-area bg-color pb-70'>
      <div className='container'>
        <div className='section-title'>
          <span className='sub-title'>
            <img src={starIcon} alt='about' />
            Miembros de Equipo
          </span>
          <h2>Sobre Nuestros Especialistas</h2>
        </div>

        <div className='row'>
          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-scientist-box'>
              <div className='image'>
                <img src={scientist2} alt='about' />
              </div>
              <div className='content'>
                <h3>Brayan Villatoro</h3>

                <ul className='social'>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-facebook'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-twitter'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-instagram'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-linkedin'></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-scientist-box'>
              <div className='image'>
                <img src={scientist2} alt='about' />
              </div>
              <div className='content'>
                <h3>Samuel Martin</h3>

                <ul className='social'>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-facebook'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-twitter'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-instagram'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-linkedin'></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-scientist-box'>
              <div className='image'>
                <img src={scientist2} alt='about' />
              </div>
              <div className='content'>
                <h3>Cristian Lopez</h3>

                <ul className='social'>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-facebook'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-twitter'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-instagram'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-linkedin'></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-scientist-box'>
              <div className='image'>
                <img src={scientist2} alt='about' />
              </div>
              <div className='content'>
                <h3>Edwin Figueroa</h3>

                <ul className='social'>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-facebook'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-twitter'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-instagram'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-linkedin'></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-scientist-box'>
              <div className='image'>
                <img src={scientist2} alt='about' />
              </div>
              <div className='content'>
                <h3>Jorge Ramírez</h3>

                <ul className='social'>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-facebook'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-twitter'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-instagram'></i>
                    </Link>
                  </li>
                  <li>
                    <Link to='#' className='d-block' target='_blank'>
                      <i className='bx bxl-linkedin'></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default TeamMembers
