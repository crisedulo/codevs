import React from 'react';
import {Link} from 'gatsby'

const FeaturedServices = () => {
    return (
        <section className="featured-services-area pt-100 pb-70">
            <div className="container">
                <div className="section-title">
                    <h2>Cómo llevamos su negocio de bueno a excelente</h2>
                    <p>Innovamos la manera actual en la que realiza sus procesos sin importar el tamaño de su organización, Seed Solutions puede ayudarlo.</p>
                </div>

                <div className="row">
                    <div className="col-lg-4 col-md-6 col-sm-6">
                        <div className="single-featured-services-box">
                            <div className="icon">
                                <i className="flaticon-analysis"></i>
                            </div>
                            <h3>
                                <Link to="/service-details">
                                    Análisis de datos
                                </Link>
                            </h3>
                            <p>Analizamos el proceso actual que realiza su empresa, descubrimos puntos clave de mejora para expandir su fortaleza.</p>

                            <Link to="/service-details" className="read-more-btn">
                                <i className="flaticon-right"></i> Leer más
                            </Link>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6 col-sm-6">
                        <div className="single-featured-services-box">
                            <div className="icon">
                                <i className="flaticon-structure"></i>
                            </div>
                            <h3>
                                <Link to="/service-details">
                                    Desarrollo de un plan personalizado
                                </Link>
                            </h3>
                            <p>Creamos una propuesta que cumple los requerimientos y explota la efectividad de los procesos actuales.</p>
                            
                            <Link to="/service-details" className="read-more-btn">
                                <i className="flaticon-right"></i> Leer más
                            </Link>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                        <div className="single-featured-services-box">
                            <div className="icon">
                                <i className="flaticon-idea"></i>
                            </div>
                            <h3>
                                <Link to="/service-details">
                                    Implementación de la solución
                                </Link>
                            </h3>
                            <p>Nos adaptamos a la tecnología existente de su empresa e implementamos la solución basandonos en una excelente comunicación y liderazgo.</p>
                            
                            <Link to="/service-details" className="read-more-btn">
                                <i className="flaticon-right"></i> Leer más
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default FeaturedServices;