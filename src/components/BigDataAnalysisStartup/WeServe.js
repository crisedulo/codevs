import React from 'react'

import VectorShape11 from '../../assets/images/shape/vector-shape11.png'

const WeServe = () => {
  return (
    <section className='industries-serve-area bg-fafafb pt-100 pb-70'>
      <div className='container'>
        <div className='section-title'>
          <h2>Industrias a las que servimos</h2>
          <p>
            Creamos software hecho a la medida según las necesidades de su
            empresa, analizamos y exploramos el proceso actual e innovamos con
            tecnología de vanguardia para que su empresa obtenga un mejor
            desempeño de su equipo y recursos.
          </p>
        </div>

        <div className='row'>
          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-industries-serve-box'>
              <div className='icon'>
                <i className='flaticon-factory'></i>
              </div>
              Fabricación
              <div className='link-btn'></div>
            </div>
          </div>

          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-industries-serve-box'>
              <div className='icon'>
                <i className='flaticon-hospital'></i>
              </div>
              Salud
              <div className='link-btn'></div>
            </div>
          </div>

          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-industries-serve-box'>
              <div className='icon'>
                <i className='flaticon-house'></i>
              </div>
              Bienes Raices
              <div className='link-btn'></div>
            </div>
          </div>

          <div className='col-lg-3 col-sm-6 col-md-6'>
            <div className='single-industries-serve-box'>
              <div className='icon'>
                <i className='flaticon-order'></i>
              </div>
              Logístico
              <div className='link-btn'></div>
            </div>
          </div>


        </div>
      </div>

      <div className='vector-shape11'>
        <img src={VectorShape11} alt='Vector Shape' />
      </div>
    </section>
  )
}

export default WeServe
