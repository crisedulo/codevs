import React from 'react'
import { Link } from 'gatsby'

const WhatWeDo = () => {
  return (
    <section className='what-we-do-area bg-fafafb pt-100 pb-70'>
      <div className='container'>
        <div className='section-title'>
          <h2>Lo que hacemos</h2>
          <p>
            Creamos un software hecho a la medida según las necesidades de su
            empresa, analisamos y exploramos el proceso actual e innovamos con
            tecnología de vanguardía para que su empresa obtenga un mejor
            desempeño de su equipo y recursos.
          </p>
        </div>

        <div className='row'>
          <div className='col-lg-4 col-md-6 col-sm-6'>
            <div className='single-what-we-do-box'>
              <div className='icon'>
                <i className='flaticon-segmentation'></i>
              </div>
              <h3>Research</h3>
              <p>
                Lorem ipsum dolor sit consectetur, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt.
              </p>

              <div className='link'>
                Market Research <i className='flaticon-right'></i>
              </div>
              <div className='link'>
                Investment Research <i className='flaticon-right'></i>
              </div>
            </div>
          </div>

          <div className='col-lg-4 col-md-6 col-sm-6'>
            <div className='single-what-we-do-box'>
              <div className='icon'>
                <i className='flaticon-analytics'></i>
              </div>
              <h3>Analytics</h3>
              <p>
                Lorem ipsum dolor sit consectetur, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt.
              </p>

              <div className='link'>
                Data Analytics <i className='flaticon-right'></i>
              </div>
              <div className='link'>
                Business Intelligence <i className='flaticon-right'></i>
              </div>
            </div>
          </div>

          <div className='col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3'>
            <div className='single-what-we-do-box'>
              <div className='icon'>
                <i className='flaticon-settings'></i>
              </div>
              <h3>Technology</h3>
              <p>
                Lorem ipsum dolor sit consectetur, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt.
              </p>

              <div className='link'>
                Intelligence Automation <i className='flaticon-right'></i>
              </div>
              <div className='link'>
                Quality Engineering <i className='flaticon-right'></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default WhatWeDo
