import React from 'react'
import { Link } from 'gatsby'

import BlogImg4 from '../../assets/images/blog/blog-img4.jpg'
import BlogImg5 from '../../assets/images/blog/blog-img5.jpg'
import BlogImg6 from '../../assets/images/blog/blog-img6.jpg'
import VectorShape1 from '../../assets/images/shape/vector-shape1.png'
import VectorShape2 from '../../assets/images/shape/vector-shape2.png'

const BlogPost = () => {
  return (
    <section className='blog-area pt-100 pb-70'>
      <div className='container'>
        <div className='section-title'>
          <h2>Cómo llevamos su negocio de bueno a excelente</h2>
          <p>
          Innovamos la manera actual en la que realiza sus procesos sin importar el tamaño de su organización, CoDevs puede ayudarlo.
          </p>
        </div>

        <div className='row'>
          <div className='col-lg-4 col-md-6'>
            <div className='single-blog-post bg-fffbf5'>
              <div className='post-image'>
                <img src={BlogImg4} alt='Blog' />
              </div>

              <div className='post-content'>
                <h3>Análisis de datos</h3>
                <span>Analizamos el proceso actual que realiza su empresa, descubrimos puntos clave de mejora para expandir su fortaleza.</span>
              </div>
            </div>
          </div>

          <div className='col-lg-4 col-md-6'>
            <div className='single-blog-post bg-fffbf5'>
              <div className='post-image'>
                <img src={BlogImg5} alt='Blog' />
              </div>

              <div className='post-content'>
                <h3>Desarrollo de un plan personalizado</h3>
                <span>Creamos una propuesta que cumple los requerimientos y explota la efectividad de los procesos actuales.</span>
              </div>
            </div>
          </div>

          <div className='col-lg-4 col-md-6 offset-lg-0 offset-md-3'>
            <div className='single-blog-post bg-fffbf5'>
              <div className='post-image'>
                <img src={BlogImg6} alt='Blog' />
              </div>

              <div className='post-content'>
                <h3>Implementación de la solución</h3>
                <span>Nos adaptamos a la tecnología existente de su empresa e implementamos la solución basándonos en una excelente comunicación y liderazgo.</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='vector-shape1'>
        <img src={VectorShape1} alt='Vector Shape' />
      </div>
      <div className='vector-shape2'>
        <img src={VectorShape2} alt='Vector Shape' />
      </div>
    </section>
  )
}

export default BlogPost
