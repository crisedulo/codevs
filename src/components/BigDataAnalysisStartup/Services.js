import React from 'react'

import ServiceShape4 from '../../assets/images/services/service-shape4.png'
import ServiceIcon13 from '../../assets/images/services/service-icon13.png'
import ServiceIcon14 from '../../assets/images/services/service-icon14.png'
import ServiceIcon15 from '../../assets/images/services/service-icon15.png'

const Services = () => {
  return (
    <section className='services-area ptb-100'>
      <div className='container'>
        <div className='section-title'>
          <h2>Servicios en los que podemos apoyarte</h2>
          <p>
            Contamos con las competencias y capacidades para innovar la manera en la que realizas tus procesos y estamos comprometidos con resolver todo tipo de problemas relacionados a la tecnología.
          </p>
        </div>

        <div className='row'>
          <div className='col-lg-4 col-md-6 col-sm-12'>
            <div className='single-services-box-item'>
              <div className='icon'>
                <img src={ServiceIcon13} alt='Service Icon' />
              </div>
              <h3>Desarrollo Web</h3>
              <p>
                Creamos sitios y sistemas web con tecnologías de vanguardía.
              </p>

              <div className='shape'>
                <img src={ServiceShape4} alt='Service Shape' />
              </div>
            </div>
          </div>

          <div className='col-lg-4 col-md-6 col-sm-12'>
            <div className='single-services-box-item'>
              <div className='icon'>
                <img src={ServiceIcon14} alt='Service Icon' />
              </div>
              <h3>RPA</h3>
              <p>
                Creamos un empleado virtual utilizando la automatización robotica de procesos.
              </p>

              <div className='shape'>
                <img src={ServiceShape4} alt='Service Shape' />
              </div>
            </div>
          </div>

          <div className='col-lg-4 col-md-6 col-sm-12'>
            <div className='single-services-box-item'>
              <div className='icon'>
                <img src={ServiceIcon15} alt='Service Icon' />
              </div>
              <h3>Aplicaciones Móviles</h3>
              <p>
                Creamos aplicaciones móviles tanto para IOS y Android con tecnologías de vanguardía. 
              </p>

              <div className='shape'>
                <img src={ServiceShape4} alt='Service Shape' />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Services
